<?php

namespace App\Http\Controllers;

use App\Classes\APICommon;
use Illuminate\Http\Request;
use App\Models\Events;
use App\Http\Resources\EventsCollection;
use Validator;

class ApiEventsController extends Controller
{
    protected function validationRules() {
        return [
            'clientName'     => 'required|max:256',
            'eventName'      => 'nullable|max:256',
            'date'         => 'required|date|date_format:Y-m-d'
        ];
    }

    public function index()
    {
        return new EventsCollection(Events::orderBy('id', 'desc')->paginate(10));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator  = Validator::make($request->all(), $this->validationRules());
        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        try {
            $result = Events::insert($request->only(['clientName','eventName','date']));
            if(!$result) {
                return response()->json('', 500);
            }
        } catch (QueryException $qx) {
            return APICommon::dbErrorHandler($qx);
        }
        return response()->json('', 201);
    }
}
