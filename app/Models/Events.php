<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
    use HasFactory;
    const CREATED_AT      = false;
    const UPDATED_AT      = false;
    protected $primaryKey = 'id';
    protected $visible    = ['id', 'clientName','eventName','date'];
    protected $fillable   = ['clientName','eventName','date'];
}
