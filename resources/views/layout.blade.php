<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>@yield('title')</title>
</head>
<body>
<div id="wrapper">
    <section id="content">
        <div class="content-wrap">
            <div class="container clearfix">
                @yield('content')
            </div>
        </div>
    </section>
</div>
</body>
</html>
