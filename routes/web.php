<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WebEventsController;

Route::get('/', [WebEventsController::class, 'index']);

Route::get('/docs', function () {
    return view('docs');
});

Route::get('/info', function () {
    return view('info');
});
