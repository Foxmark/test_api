<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class EventsTest extends TestCase
{
    use WithFaker;
    use DatabaseMigrations;

    public function testEventsValidation()
    {
        $response1 =  $this->post('/api', []);
        $response1->assertStatus(400);
        $response1->assertSee('The client name field is required.');
        $response1->assertSee('The date field is required.');
    }

    public function testEventsDateValidation()
    {
        $response1 =  $this->post('/api', ['clientName' => 'validClient', 'eventName' => 'validName', 'date' => '2020-15-55']);
        $response1->assertStatus(400);
        $response1->assertSee('The date is not a valid date.');
        $response1->assertSee('The date does not match the format Y-m-d.');
    }

    public function testEventsReturnFormat()
    {
        $request = $this->get('/api');
        $request->assertStatus(200);

        $return_data =  $request->decodeResponseJson();
        $this->assertArrayHasKey('data', $return_data);
        $this->assertArrayHasKey('links', $return_data);
        $this->assertArrayHasKey('meta', $return_data);
    }

    public function testEventspagination()
    {
        $request = $this->get('/api');
        $request->assertStatus(200);

        $return_data =  $request->decodeResponseJson();
        $this->assertArrayHasKey('per_page', $return_data['meta']);
        $this->assertEquals(10, $return_data['meta']['per_page'], 'per_page value should be 10');
    }

    public function testEventsEndpoint()
    {
        $data = [
            'clientName' => $this->faker->text(15),
            'eventName'  => $this->faker->text(30),
            'date'       => '2020-12-31'
        ];

        $request = $this->post('/api', $data);
        $request->assertStatus(201);

        $this->get('/api')->assertSee($data['eventName']);
        $this->assertDatabaseHas('events', $data);

        $response = $this->get('/api');
        $response->assertStatus(200);
    }
}
